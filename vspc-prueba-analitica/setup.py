""" Setup file """
from setuptools import setup
import versioneer


setup(    
    name = 'vspc-prueba-analitica',
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    extras_require = {
        ':python_version<=\'3.7\'':  [
            'pyodbc==4.0.27'
        ],
        ':python_version>\'3.7\'':  [
            'pyodbc>=4.0.35'
        ]
    }
)
