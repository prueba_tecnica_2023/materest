# -*- coding: utf-8 -*-

"""
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-- Equipo Vicepresidencia de Servicios para los Clientes
-----------------------------------------------------------------------------
-- Fecha Creación: 20230728
-- Última Fecha Modificación: 20230728
-- Autores: materest
-- Últimos Autores: materest
-- Descripción: Script de ejecución de los ETLs
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
"""
from orquestador2.step 	    import Step
from datetime	       		import datetime
from dateutil.relativedelta import relativedelta
import json
import pkg_resources
import os
import pandas as pd

class Subir_Insumo(Step):
    def ejecutar(self):
        
        nombre_excel = ['Obligaciones_clientes.xlsx','tasas_productos.xlsx']
        nombre_lz = ['obligaciones_clientes_materest','tasas_productos_materest']
        ruta = self.getFolderPath() + "insumos/"  
        
        self.subir_insumos(nombre_excel,nombre_lz,ruta)
        
    def subir_insumos(self,nombre_excel,nombre_lz,ruta_insumo):
        posicion_lz = 0
        posicion_xls = 0
        
        sparky = self.getSparky()
        for i in range(len(nombre_excel)):
            df = pd.read_excel(ruta_insumo + nombre_excel[posicion_xls])
            posicion_xls = posicion_xls +1
            sparky.subir_df(df, nombre_tabla= nombre_lz[posicion_lz], zona= "proceso",modo="overwrite")
            posicion_lz = posicion_lz + 1
            
            
class ETL(Step):
    
    def ejecutar(self):       
        ruta = self.getFolderPath() + "sql/"
        parametros = self.getGlobalConfiguration()["parametros_lz"]
        self.executeFolder(folder = ruta, params = parametros)
            
class ExtractTransformLoad(Step):
    """
    Clase encargada de la ejecución de los ETLs
    necesarios para extraer y procesar la información
    de interés de la rutina.
    """

    @staticmethod
    def obtener_ruta():
        """
        Función encargada de identificar la
        carpeta static relacionada al paquete
        ------
        Return
        ------
        ruta_src : string
        Ruta static en el sistema o entorno de
        los recursos del paquete
        """
        return pkg_resources.resource_filename(__name__, 'static')

    def obtener_params(self):
        """
        Función encargada de obtener los parámetros
        necesarios para la ejecución del paso.
        ------
        Return
        ------
        params : dictionary
        Parámetros necesarios para ejecutar el paso.
        """
        #PARAMETROS GENERALES DEL PASO
        params = self.getGlobalConfiguration()["parametros_lz"]
        now = datetime.today()
        params_default = {
            "kwargs_year"  : now.year,
            "kwargs_month" : now.month,
            "kwargs_day"   : now.day
        }
        params_default.update(self.kwa)
        now = datetime(
            params_default["kwargs_year"]
            , params_default["kwargs_month"]
            , params_default["kwargs_day"]
        )
        params_calc = {
            #FECHAS
            "f_corte_y"  : \
                str((now + relativedelta(months=-1)).year),
            "f_corte_m"  : \
                str((now + relativedelta(months=-1)).month),
            "f_corte_d"  : \
                str((now + relativedelta(months=-1)).day),
            "f_actual_y" : \
                str(now.year),
            "f_actual_m" : \
                str(now.month),
            "f_actual_d" : \
                str(now.day)
        }
        params.update(params_calc)
        params.update(self.kwa)
        params.pop("password", None)
        return params

    def ejecutar(self):
        """
        Función que ejecuta el paso de la clase.
        """
        self.log.info(json.dumps(
            self.obtener_params(), \
            indent = 4, sort_keys = True))
        self.executeTasks()

    def ejecutar_modulos(self):
        """
        Función que ejecuta los módulos de información.
        """
        self.executeFolder(self.getSQLPath() + \
            type(self).__name__, self.obtener_params())

class Salidas(Step):
    def ejecutar(self):
    
        help = self.getHelper() 
        ruta = self.getFolderPath() + "salidas/" 
        #punto 3
        df1 = help.obtener_dataframe(consulta = "SELECT * FROM proceso.pa_sql_parte1_pto_3;")
        #punto 4
        df2 = help.obtener_dataframe(consulta = "SELECT * FROM proceso.pa_sql_parte1_pto_4;")
        
        df1.to_csv(ruta + 'punto_3.csv', index = False)
        df2.to_csv(ruta + 'punto_4.csv',  index = False)
   