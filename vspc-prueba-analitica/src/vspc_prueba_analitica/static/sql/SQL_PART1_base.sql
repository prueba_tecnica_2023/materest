-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-- Equipo Vicepresidencia de Servicios para los Clientes
-----------------------------------------------------------------------------
-- Fecha Creación: 20230728
-- Última Fecha Modificación: 20230728
-- Autores: materest
-- Últimos Autores: materest
-----------------------
---

/* punto SQL PARTE 1:
1. se genera la base final donde sera utilizada en los puntos 1,2,3 y 4 :*/

DROP TABLE IF EXISTS {zona_procesamiento}.{prefijo}sql_parte1_base PURGE;
CREATE TABLE IF NOT EXISTS {zona_procesamiento}.{prefijo}sql_parte1_base STORED AS PARQUET TBLPROPERTIES ('transactional'='false') AS

with obligaciones_clientes as (
select t1.num_documento, t1.id_producto,
case 
when 
trim(substring(SUBSTRING(t1.id_producto, instr(t1.id_producto, ' - ') +2),instr(SUBSTRING(t1.id_producto, instr(t1.id_producto, ' - ') +2),'-')+1 )) = 'Cartera Total' 
    then 'cartera'
when trim(substring(SUBSTRING(t1.id_producto, instr(t1.id_producto, ' - ') +2),instr(SUBSTRING(t1.id_producto, instr(t1.id_producto, ' - ') +2),'-')+1 )) LIKE 'Tarjeta de Cr%'
    THEN 'cartera' 
when trim(substring(SUBSTRING(t1.id_producto, instr(t1.id_producto, ' - ') +2),instr(SUBSTRING(t1.id_producto, instr(t1.id_producto, ' - ') +2),'-')+1 )) LIKE '%Leasing Cartera T%'
    THEN 'leasing'  
when trim(substring(SUBSTRING(t1.id_producto, instr(t1.id_producto, ' - ') +2),instr(SUBSTRING(t1.id_producto, instr(t1.id_producto, ' - ') +2),'-')+1 )) LIKE '%Sufi%'
    THEN 'sufi'    
    else trim(substring(SUBSTRING(t1.id_producto, instr(t1.id_producto, ' - ') +2),instr(SUBSTRING(t1.id_producto, instr(t1.id_producto, ' - ') +2),'-')+1 )) end as producto,
t1.tipo_id_producto,
t1.cod_segm_tasa, t1.cod_subsegm_tasa, t1.cal_interna_tasa, t1.cod_periodicidad, valor_inicial
from proceso.obligaciones_clientes_materest t1
),
tasa_productos as (
select 
t2.cod_segmento,t2.cod_subsegmento,t2.calificacion_riesgos,
t2.tasa_cartera,	t2.tasa_operacion_especifica,	t2.tasa_hipotecario,
t2.tasa_leasing,	t2.tasa_sufi,	t2.tasa_factoring,	t2.tasa_tarjeta
from proceso.tasas_productos_materest t2
),
unir as (
select num_documento, cod_segm_tasa, cod_subsegm_tasa,cal_interna_tasa, producto,
(case 
when trim(producto) = 'operacion_especifica' then tasa_operacion_especifica
when trim(producto) = 'leasing' then tasa_leasing
when trim(producto) = 'cartera' then tasa_cartera
when trim(producto) = 'tarjeta' then tasa_tarjeta
when trim(producto) = 'Hipotecario' then tasa_hipotecario
when trim(producto) = 'leasing' then tasa_leasing
when trim(producto) = 'sufi' then tasa_sufi
when trim(producto) = 'factoring' then tasa_factoring
else 0 end ) as tasa_x_producto,
cod_periodicidad,
valor_inicial

from obligaciones_clientes
left join tasa_productos
on (cod_segm_tasa = cod_segmento and cod_subsegm_tasa = cod_subsegmento
and cal_interna_tasa = calificacion_riesgos)
),
tabla_te as (
SELECT distinct *,
(((power(1 + tasa_x_producto,(1/cod_periodicidad)) -1 ) * cod_periodicidad) / cod_periodicidad) as tasa_efectiva
FROM unir
)
select *, (tasa_efectiva * valor_inicial) as valor_final
from tabla_te
;


COMPUTE STATS {zona_procesamiento}.{prefijo}sql_parte1_base;
--------------------------------- Query End ---------------------------------