-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-- Equipo Vicepresidencia de Servicios para los Clientes
-----------------------------------------------------------------------------
-- Fecha Creación: 20230728
-- Última Fecha Modificación: 20230728
-- Autores: materest
-- Últimos Autores: materest
-----------------------
---
/* punto SQL PARTE 1:
1. Se requiere tomar las obligaciones de cada cliente y agregar la tasa correspondiente al 
producto asignado*/

DROP TABLE IF EXISTS {zona_procesamiento}.{prefijo}sql_parte1_pto_1 PURGE;
CREATE TABLE IF NOT EXISTS {zona_procesamiento}.{prefijo}sql_parte1_pto_1 STORED AS PARQUET TBLPROPERTIES ('transactional'='false') AS
select distinct num_documento, cod_segm_tasa, cod_subsegm_tasa,cal_interna_tasa, producto,
tasa_x_producto
from {zona_procesamiento}.{prefijo}sql_parte1_base 
order by num_documento asc;

COMPUTE STATS {zona_procesamiento}.{prefijo}sql_parte1_pto_1;
--------------------------------- Query End ---------------------------------