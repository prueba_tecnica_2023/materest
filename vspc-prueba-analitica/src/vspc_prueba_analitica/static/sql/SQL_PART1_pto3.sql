-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-- Equipo Vicepresidencia de Servicios para los Clientes
-----------------------------------------------------------------------------
-- Fecha Creación: 20230728
-- Última Fecha Modificación: 20230728
-- Autores: materest
-- Últimos Autores: materest
-----------------------
---
/* punto SQL PARTE 1:
3. Tomar la tasa efectiva, multiplicarla por el valor_inicial y dejar este resultado como 
valor_final, el resultado de esta tabla debe quedar almacenado; ya que este nos 
servirá como un insumo de la parte 2:*/

DROP TABLE IF EXISTS {zona_procesamiento}.{prefijo}sql_parte1_pto_3 PURGE;
CREATE TABLE IF NOT EXISTS {zona_procesamiento}.{prefijo}sql_parte1_pto_3 STORED AS PARQUET TBLPROPERTIES ('transactional'='false') AS

select distinct num_documento, cod_segm_tasa, cod_subsegm_tasa,cal_interna_tasa, producto,
tasa_x_producto,cod_periodicidad, tasa_efectiva, valor_inicial, valor_final
from proceso.pa_sql_parte1_base
order by num_documento asc;
;

COMPUTE STATS {zona_procesamiento}.{prefijo}sql_parte1_pto_3;
--------------------------------- Query End ---------------------------------