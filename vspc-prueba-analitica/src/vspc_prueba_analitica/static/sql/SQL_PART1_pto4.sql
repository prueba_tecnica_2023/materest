-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-- Equipo Vicepresidencia de Servicios para los Clientes
-----------------------------------------------------------------------------
-- Fecha Creación: 20230728
-- Última Fecha Modificación: 20230728
-- Autores: materest
-- Últimos Autores: materest
-----------------------
---
/* punto SQL PARTE 1:
4. Se necesita sumar el valor_final de todas las obligaciones por cliente y dejar 
únicamente las que tenga un valor superior a … el resultado de esta tabla debe 
quedar almacenado; ya que este nos servirá como un insumo de la parte 3.2.*/

DROP TABLE IF EXISTS {zona_procesamiento}.{prefijo}sql_parte1_pto_4 PURGE;
CREATE TABLE IF NOT EXISTS {zona_procesamiento}.{prefijo}sql_parte1_pto_4 STORED AS PARQUET TBLPROPERTIES ('transactional'='false') AS
with a as (
select distinct num_documento, cod_segm_tasa, cod_subsegm_tasa,cal_interna_tasa, producto,
tasa_x_producto,cod_periodicidad, tasa_efectiva, valor_inicial, valor_final
from proceso.pa_sql_parte1_base
order by num_documento asc
)
select  num_documento, sum(valor_final) as suma_valor_final
from a 
group by num_documento
;

--por que valor filtro ?
--where (valor_final) > a

COMPUTE STATS {zona_procesamiento}.{prefijo}sql_parte1_pto_4;
--------------------------------- Query End ---------------------------------