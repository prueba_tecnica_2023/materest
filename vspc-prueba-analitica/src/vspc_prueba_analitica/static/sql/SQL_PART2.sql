-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-- Equipo Vicepresidencia de Servicios para los Clientes
-----------------------------------------------------------------------------
-- Fecha Creación: 20230728
-- Última Fecha Modificación: 20230728
-- Autores: materest
-- Últimos Autores: materest
-----------------------
---

/* punto SQL PARTE 2:

A). 4%) Seleccionar todos los clientes.
RPTA: 
SELECT * FROM CLIENTE;

B). 4%) Seleccionar los clientes que pertenezcan a la región “Centro”. 
SELECT * FROM CLIENTE
WHERE trim(Region) = "Centro"

C). 4%) Seleccionar los clientes que tengan más de 3 cuentas abiertas en estado Activo. 
SELECT CEDULA , COUNT(*) AS CUENTAS_ACTIVAS
FROM CLIENTE INNER JOIN CUENTA
ON (CEDULA = CEDULA_CLIENTE)
WHERE Estado = 'Activo'
GROUP BY CEDULA
HAVING COUNT(*) > 3;


D). (4%) Seleccionar solamente el nombre de aquellos clientes que tienen clave dinámica
SELECT Nombre 
FROM CLIENTE 
INNER JOIN CLAVE_DINAMICA
ON (Cedula = cedula_cliente)

E). (8%) Seleccionar los clientes que no tienen clave dinámica. 
SELECT Nombre 
FROM CLIENTE 
LEFT JOIN CLAVE_DINAMICA
ON (Cedula = cedula_cliente)
WHERE CLAVE_DINAMICA IS NULL

F). (8%) Mostrar el saldo total de todas las cuentas agrupado por la región del cliente
SELECT cedula,Region, SUM(Saldo) AS SALDO_TOTAL
FROM CLIENTE INNER JOIN CUENTAS  
ON (Cedula = Cedula_cliente)
GROUP BY 1,2;

G) 8%) Seleccionar el sado total de las cuentas activas, abiertas en el mes de mayo de 2018, 
cuyos clientes tengan clave dinámica. 

WITH CLIENTE_CD AS (
    SELECT Nombre, Cedula
    FROM CLIENTE 
    INNER JOIN CLAVE_DINAMICA
    ON (Cedula = cedula_cliente)
    WHERE (correo IS NOT NULL OR celular IS NOT NULL)
),
cuentas as (
SELECT Cedula_cliente, Num_cuenta, SUM(Saldo) AS SALDO_TOTAL
FROM  CUENTAS  
WHERE Estado = 'Activa' and Fecha_apertura between '01/05/2018' and '31/05/2018'
group by 1, 2
)
SELECT NUM_CUENTA, CEDULA_CLIENTE, SALDO_TOTAL
FROM CLIENTE_CD INNER JOIN CUENTAS
ON (CEDULA = CEDULA_CLIENTE);


*/

